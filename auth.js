//we will use this for authentication
	//login
	//retrieve user details

//Json Web Token
	//methods:
		//sign(data, secret, {})
			//creates a token
		//verify(token, secret, cb())
			//checks if the token is present
		//decode(token, {}).payload
			//interpret/decodes the created token

let jwt = require('jsonwebtoken');
let secret = "CourseBookingAPI";

//create token
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//verify token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	console.log(token); //bearer jkhsdjgslkdfh'psuigjkad

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"})
			} else {
				next();
			}
		})

	}
}



//decode token
module.exports.decode = (token) => {


	if (typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}
	//if there's a presence of token, return decoded token
		//make sure to remove "bearer" words from the token
}

//decode(req.headers.authorization)