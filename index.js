const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000; // pag mag hohost na
const app = express();
const cors = require('cors');

//routes
let userRoutes = require("./routes/userRoutes");

//adding course step 3
let courseRoutes = require("./routes/courseRoutes");

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://ppr0119:admin@cluster0.pzreh.mongodb.net/course_booking?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(()=>console.log(`Connected to Database`))
.catch( (error)=> console.log(error))


//schema

//routes
app.use("/api/users", userRoutes);

//adding course step 4
app.use("/api/courses", courseRoutes);

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));
