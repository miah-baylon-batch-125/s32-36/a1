const Course = require('./../models/Courses')

module.exports.getAllActive = () => {
	//Model.method
 	return Course.find({isActive:true}).then( result => result)

}

//part2 get all courses
module.exports.getAllCourses = () => {
	//model.method
	return Course.find().then( result => {
		return result
	})
}

//adding course step 2
module.exports.addCourse = (reqBody) => {
	//Model.prototype.method
	//see mongoose documentation

//adding course step 5
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})
	//Model.prototype.method
	return newCourse.save().then( (course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
} 

//make a route to get single course step 2
module.exports.getSingleCourse = (params) => {
	//console.log(params) //{ courseId: '6131b2f45528f7bcdda85a56' }
	//model.method
	return Course.findById(params.courseId).then( course => {
		return course
	})
}

//edit course
module.exports.editCourse = (params, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//archive course
module.exports.archiveCourse = (params) => {

	let updatedActiveCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


// unarchive course

module.exports.unarchiveCourse = (params) => {

	let updatedActiveCourse = {
		isActive: true
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//delete course


module.exports.deleteCourse = (params) => {

	return Course.findByIdAndDelete(params)
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
