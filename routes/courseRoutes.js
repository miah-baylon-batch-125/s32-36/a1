const express = require('express');
const router = express.Router();
let auth = require('./../auth');


//controllers

const courseController = require('./../controllers/courseControllers');

//retrieve active courses
router.get('/active', (req, res) => {
	courseController.getAllActive().then( result => res.send(result))


})

//retrieve all courses //part1 get all courses
router.get("/all", auth.verify, (req, res) => {
	courseController.getAllCourses().then( result => res.send(result))
})


//add course
//create a route and to add course and return true if course is successfully saved else return false if not

//adding course step 1
router.post('/addCourse', auth.verify, (req, res) => {
	courseController.addCourse(req.body).then(result => res.send(result));
})

//make a route to get single course
//make a route to get single course step 1
router.get('/:courseId', auth.verify, (req,res) => {
	
	courseController.getSingleCourse(req.params).then(result => res.send(result))
})

//make a route to update the course
router.put('/:courseId/edit', auth.verify, (req,res) => {
	courseController.editCourse(req.params.courseId, req.body).then (result => 
		res.send(result)
	)

})


//make a route to archive the course
	// meaning update the status of the course from true to false

router.put('/:courseId/archive', auth.verify, (req, res) => {

	courseController.archiveCourse(req.params.courseId).then(result => res.send(result))
})

//make a route to unacrchive the course
	//meaning update the status of the course from false to true

router.put('/:courseId/unarchive', auth.verify, (req, res) => {

	courseController.unarchiveCourse(req.params.courseId).then(result => res.send(result))
})

//make a route to delete a course
	//meaning, the course object will be deleted from the data base


//delete course
router.delete('/:courseId/delete', auth.verify,(req,res)=>{
	//console.log(req.params.courseId)
	courseController.deleteCourse(req.params.courseId).then(result => res.send(result));
})



module.exports = router;